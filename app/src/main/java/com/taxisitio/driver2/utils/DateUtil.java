package com.taxisitio.driver2.utils;

import com.taxisitio.driver2.activity.BaseActivity;

/**
 * Ayudante para las fechas
 */
public class DateUtil {
    private final static String FORMAT_SERVER = "yyyy-MM-dd hh:mm:ss";
    private final static String FORMAT_CLIENT = "dd/MMM/yyyy, hh:mm a";

    public static String formatDate(BaseActivity context,String date){
        String r = "NA";
        if(!date.equalsIgnoreCase("")){
            r = context.changeDateFormatFromUTCdate(date, FORMAT_SERVER, FORMAT_CLIENT);
        }
        return r;
    }
}
