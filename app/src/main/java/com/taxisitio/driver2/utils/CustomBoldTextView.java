package com.taxisitio.driver2.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by TOXSL\ankush.walia on 27/1/17.
 */

public class CustomBoldTextView extends TextView {

    public CustomBoldTextView(Context context) {
        super(context);
        setFont();
    }

    public CustomBoldTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFont();
    }

    public CustomBoldTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setFont();
    }

    private void setFont() {
        Typeface font = Typeface.createFromAsset(getContext().getAssets(), "fonts/gotham_Narrow_Book.otf");
        setTypeface(font, Typeface.BOLD);
    }
}