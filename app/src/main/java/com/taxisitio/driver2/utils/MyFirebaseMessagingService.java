package com.taxisitio.driver2.utils;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.core.app.NotificationCompat;
import androidx.core.app.TaskStackBuilder;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.activity.SplashActivity;

import java.util.Map;

/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMessaging";

    public void displayMessage(Context context, Bundle extras) {
        Intent intent = new Intent(Const.DISPLAY_MESSAGE_ACTION);
        intent.putExtra("map", extras);
        context.sendBroadcast(intent);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e("bundle Data", "" + remoteMessage);
        Map data = remoteMessage.getData();
        Log.e("bundle Data", "" + data);
        String from = remoteMessage.getFrom();
        Log.d(TAG, "From: " + from);
        PrefStore mPrefStore = new PrefStore(this);
        Bundle bundle = new Bundle();
        bundle.putString("action", data.get("action").toString());
        bundle.putString("controller", data.get("controller").toString());
        bundle.putString("message", data.get("message").toString());
        if (data.containsKey("ride_id"))
            bundle.putString("id", data.get("ride_id").toString());
        generateNotification(this, bundle);
        displayMessage(this, bundle);
    }

    public void generateNotification(Context context, Bundle extra) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String NOTIFICATION_CHANNEL_ID = "tsd_02";

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "My Notifications",NotificationManager.IMPORTANCE_HIGH);
            // Configure the notification channel.
            notificationChannel.setDescription("Notificaciones");
            notificationManager.createNotificationChannel(notificationChannel);
        }

        String notificationMessage = (String) extra.get("message");
        int noti_id = 0;
        Bitmap largeIcon = BitmapFactory.decodeResource(this.getResources(), R.mipmap.notification_1);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,NOTIFICATION_CHANNEL_ID)
                .setSmallIcon(R.mipmap.notification_1)
                .setContentTitle(this.getResources().getString(R.string.app_name)).setContentText(notificationMessage).setAutoCancel(true);
        Uri NotiSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mBuilder.setSound(NotiSound);
        mBuilder.setLargeIcon(largeIcon);
        mBuilder.setAutoCancel(true);
        long[] vibrate = {0, 100, 200, 300};
        mBuilder.setVibrate(vibrate);
        Intent resultIntent = new Intent(context, MainActivity.class);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra("map", extra);
        resultIntent.putExtra("isPush", true);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(SplashActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        {
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(noti_id, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(noti_id, mBuilder.build());
        }
    }

    // [START on_new_token]

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    public void onNewToken(String token) {
        super.onNewToken(token);
        //Util.Log(TAG, "Refreshed token: " + token);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(token);

        Log.d(TAG, "Refreshed token: " + token);
        Intent intent = new Intent(Const.FCM_TOKEN_REFRESH);
        intent.putExtra(Const.FCM_TOKEN_REFRESH, token);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
    // [END on_new_token]
}