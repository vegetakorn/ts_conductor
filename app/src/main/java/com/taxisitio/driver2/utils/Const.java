package com.taxisitio.driver2.utils;

public class Const {

    //public static final String SERVER_REMOTE_URL = "http://192.168.53.10:8080/freelance/ludwins/taxiwepp/app/api/";
    //public static final String SERVER_REMOTE_URL = "http://192.168.53.30:8080/freelance/ludwins/taxiwepp/app/api/";
    //public static final String SERVER_REMOTE_URL = "http://172.16.60.30:8080/freelance/ludwins/taxibloque/app/api/";
    public static final String SERVER_REMOTE_URL = "http://74.91.126.73:8080/api/";

    public static final String API_USER_DRIVER_UPDATE = "user/driver-update";
    public static final String API_DRIVER_VEHICLE_UPDATE = "driver/vehicle-update";

    public static final String DISPLAY_MESSAGE_ACTION = "com.taxisitio.driver.DISPLAY_MESSAGE";
    public static final int PLAY_SERVICES_RESOLUTION_REQUEST = 1234;
    /**
     * ******************************* App buy or Purchase Key *************************************
     */
    public static final String IN_APP_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiwmkXRr/Ui6Onc3hZl8gvxe2D9DIyAELLBMryE2+iz1X6ToPAl2D+Lypytja+xc5u1s5yyfX5PzsruerkUfwm6nMjXwX2v64+syV3QjC1yQ3HuMCL4IJgu+jSymj8PQ+tBK9i8ehWm4NvYZ8KG6uCXnR2xqWQKgJEm0E96aMl7jwb7BvAvfn62/Frqlxw9n3V1f11CbgbQUbY8rOjpZvTZ+LF+Ly5NdFAM+H8BafdF7WQ0N3mmQfqmEBLamiqS/+5F5nGjSYrn2cjjBCbfDlQMxUS0fSjACQm+Ri6r5Qzi+cYyYt9Zn/i/U6Aimj/9qyoDwTV3TTMm2tv7lVn7VLnQIDAQAB";
    public static final int GALLERY_REQ = 1;
    public static final int CAMERA_REQ = 2;
    public static final String TITLE = "title";
    public static final String ROW_ID = "news_id";
    public static final String FCM_TOKEN_REFRESH = "com.taxisitio.passenger.FCM_TOKEN_REFRESH";
    public static final String IS_AVAILABLE = "is_available";
    /**
     * Journry States------------------------------------Start
     */
    public static final int RIDE_NOW = 1;
    public static final int RIDE_LATER = 2;
    public static final int NOT_HOURLY = 0;
    public static final int IS_HOURLY = 1;

    public static final int STATE_NEW = 1;
    public static final int STATE_ACCEPTED = 2;
    public static final int STATE_REJECTED = 3;
    public static final int STATE_CANCELLED = 4;
    public static final int STATE_DRIVER_ARRIVED = 5;
    public static final int STATE_STARTED = 6;
    public static final int STATE_COMPLETED = 7;
    public static final int STATE_PAID = 8;
    public static final String STATE_ID = "state_id";
    /**
     * Journry States---------------------------------------End
     */


    public static final String API_STEP_1 = "user/driver-step1";
    public static final String API_STEP_2 = "user/driver-step2";
    public static final String API_LOGIN = "user/login";
    public static final String API_USER_SET_LOCATION = "user/set-location";
    public static final String API_RIDE_ACCEPT = "ride/accept?id=";
    public static final String API_RIDE_ARRIVED = "ride/arrived?id=";
    public static final String API_RIDE_STARTED = "ride/started?id=";
    public static final String API_RIDE_COMPLETED = "ride/completed?id=";

    /**
     * Constants---------------------------------------start
     */
    public static final int REQ_CODE_PROFILE_IMAGE = 1;
    public static final int REQ_CODE_VEHICLE_DOC_IMAGE = 2;
    public static final int REQ_CODE_LICENCE_IMAGE = 3;
    public static final int REQ_CODE_ID_PROOF_IMAGE = 4;
    public static final int REQ_CODE_VEHICLE_IMAGE = 9;
    public static final int DRIVER_AVAILABLE = 1;
    public static final int ROLE_DRIVER = 2;
    public static final String JOURNEY_ID = "journey_id";
    public static final int ALL_NOTI = 5046;
    public static final int ABOUT_PAGE = 0;
    public static final int TC_CUSTOMER_PAGE = 1;
    public static final int TC_DRIVER_PAGE = 2;
    /**
     * Constants---------------------------------------end
     */
    public static final String API_DRIVER_PROFILE = "driver/profile";
    public static final String API_USER_RECOVER = "user/recover";
    public static final String API_USER_CHANGE_PASSWORD = "user/change-password";
    public static final String DRIVER_STATE = "driver-state";
    public static final String API_RIDE_DETAIL = "ride/detail";

    public static final String API_PAGE_GET = "page/get";//?type_id=
    public static final String API_RIDE_DRIVER_RIDE_HISTORY = "ride/driver-ride-history";

    //Lista las empresas
    public static final String API_COMPANY_INDEX = "company/index";

    /**
     * Type of cars
     */
    public static final int CAR = 0;
    public static final int SUV = 1;
    public static final int Minivan = 2;
    public static final String TYPE_CAR = "Automovil";
    public static final String TYPE_SUV = "SUV";
    public static final String TYPE_MINIVAN = "MiniVan";


}