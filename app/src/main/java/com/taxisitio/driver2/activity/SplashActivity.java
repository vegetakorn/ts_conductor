package com.taxisitio.driver2.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.taxisitio.driver2.R;
import com.taxisitio.driver2.data.ProfileData;
import com.taxisitio.driver2.data.RideDetails;

import org.json.JSONException;
import org.json.JSONObject;

import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;
import com.taxisitio.driver2.utils.Util;


/**
 * Created by TOXSL\visha.sehgal on 12/1/17.
 */

public class SplashActivity extends BaseActivity {
    private Handler handler;
    private Bundle bundle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(Util.isDebug() == false){
            AppCenter.start(getApplication(), "bb2c97e7-2839-4812-8616-44d092ddf2e8",
                    Analytics.class, Crashes.class);
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        keyHash();
        if (getIntent() != null && getIntent().hasExtra("map")) {
            if (getIntent().getBooleanExtra("isPush", false)) {
                bundle = getIntent().getBundleExtra("map");
            }
        }
        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (checkPlayServices())
                    check();
            }
        }, 2000);
    }

    protected void allParent() {
        check();
        super.allParent();

    }
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("check")) {
            if (status) {
                JSONObject object = null;
                try {
                    object = jsonObject.getJSONObject("detail");
                    ProfileData driverData = parseProfileData(object);
                    Intent intent = new Intent(this, MainActivity.class);
                    if (bundle != null) {
                        intent.putExtra("map", bundle);
                    }
                    assert object != null;
                    if (object.has("last_ride")) {
                        RideDetails rideDetails = parseRideDetails(object.getJSONObject("last_ride"));
                        if (rideDetails != null) {
                            intent.putExtra("rideDetail", rideDetails);
                        }

                    }
                    saveProfileDataInPrefStore(driverData);
                    if (driverData.driver_profile_step == 2) {
                        startActivity(intent);
                    } else {
                        Intent signup = new Intent(this, LoginSignupActivity.class);
                        intent.putExtra("driver_profile_step", driverData.driver_profile_step);
                        startActivity(signup);
                    }
                    finish();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } else {
                gotoLogin();
            }
        }

    }
}
