package com.taxisitio.driver2.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxisitio.driver2.R;
import com.taxisitio.driver2.fragment.LoginSignupFragment;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */


public class LoginSignupActivity extends BaseActivity {
    public Toolbar toolbar;
    public TextView toolbar_titleTV;
    public ImageView toolbar_imageIV;
    private int driver_profile_step;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);
        setToolbar();
        if (getIntent() != null) {
            driver_profile_step = getIntent().getIntExtra("driver_profile_step", 0);
        }
        goToLoginSignUpScreen();
    }

    private void goToLoginSignUpScreen() {
        getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new LoginSignupFragment()).addToBackStack(null).commit();
    }

    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof LoginSignupFragment)
            back();
        else if (getSupportFragmentManager().getBackStackEntryCount() > 0)
            getSupportFragmentManager().popBackStack();
    }

    public void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar_titleTV = (TextView) findViewById(R.id.toolbar_titleTV);
        toolbar_imageIV = (ImageView) findViewById(R.id.toolbar_imageIV);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("");
        toolbar_imageIV.setVisibility(View.GONE);
        toolbar.setBackgroundColor(ContextCompat.getColor(this,R.color.bg_color));
        toolbar_titleTV.setTextColor(ContextCompat.getColor(this,R.color.White));
        toolbar.setVisibility(View.VISIBLE);
        toolbar_titleTV.setVisibility(View.VISIBLE);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        Drawable upArrow = getResources().getDrawable(R.mipmap.ic_back_black);
        upArrow.setColorFilter(ContextCompat.getColor(this,R.color.White), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

    }

    public void setToolbarTitle(String title) {
        toolbar_titleTV.setVisibility(View.VISIBLE);
        toolbar_imageIV.setVisibility(View.GONE);
        toolbar_titleTV.setText(title);
    }
}
