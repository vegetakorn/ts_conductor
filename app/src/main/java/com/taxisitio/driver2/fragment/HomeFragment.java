package com.taxisitio.driver2.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.appcompat.app.AlertDialog;

import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.BaseActivity;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.data.ProfileData;
import com.taxisitio.driver2.service.LocationUpdateService;
import com.taxisitio.driver2.utils.Const;
import com.taxisitio.driver2.utils.LatLngInterpolator;
import com.taxisitio.driver2.utils.MarkerAnimation;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class HomeFragment extends BaseFragment implements OnMapReadyCallback, BaseActivity.PermCallback, LocationUpdateService.OnLocationReceived {
    private static final int SETUP_MAP_LOCATION_PERM_REQ = 5;
    private static final int AVAILABLE = 1;
    GoogleMap googlemap;
    ProfileData profileData;
    private AlertDialog.Builder alert;
    ImageView toolbar_imageIV;
    private View view;
    private int car_Type;
    private Marker mymarker;
    private Location currentLocation;
    private int count = 0;
    private String TAG = "HomeFragment";
    private boolean isInitPosition = false;
    private Runnable task = new Runnable() {
        @SuppressLint("MissingPermission")
        @Override
        public void run() {
            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            if (!isInitPosition) {
                                isInitPosition = true;
                                currentLocation = location;
                            }
                            setCurrentLocation();
                        }
                    });
        }
    };
    private Timer timer;
    private boolean firstTimeFlag = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        ((MainActivity) baseActivity).setToolbar(getString(R.string.taxi_bloque));
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);

        }
        try {
            view = inflater.inflate(R.layout.fragment_home, container, false);
        } catch (InflateException e) {
            e.printStackTrace();
        }

        profileData = baseActivity.getProfileDataFromPrefStore();
        init();


        return view;
    }


    private void init() {
        initilizeMap();
    }


    public void setMyCar() {
        car_Type = Integer.parseInt(baseActivity.getProfileDataFromPrefStore().vehicle);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (locationUpdateService != null)
            locationUpdateService.setLocationReceivedListner(this);
        setMyCar();
        if (currentLocation != null) {
            setMyLocation(currentLocation);
            startTimer();
        }
    }

    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            map.getMapAsync(this);
        } else {
            setCurrentLocation();
        }
    }

    private void setCurrentLocation() {
        if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, SETUP_MAP_LOCATION_PERM_REQ, this)) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            currentLocation = location;
                            if (currentLocation != null) {
                                if (currentLocation != null) {
                                    setMyLocation(currentLocation);
                                    //sendLocationUpdate(currentLocation);
                                    count = 0;
                                } else {
                                    if (count < 5) {
                                        count++;
                                        setCurrentLocation();
                                    } else {
                                        showToast(getString(R.string.cannot_get_location_please_refresh));
                                    }
                                }
                            } else {
                                showToast(getString(R.string.couldnt_get_current_location));
                            }
                        }
                    });
        }
    }


    private void setMyLocation(Location currentLocation) {
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        if (mymarker == null) {
            if (googlemap != null) {
                MarkerOptions markerOptions = new MarkerOptions();
                car_Type = Integer.valueOf(profileData.vehicle);
                if (car_Type == Const.CAR)
                    markerOptions.position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car));
                else if (car_Type == Const.SUV)
                    markerOptions.position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_suv));
                else
                    markerOptions.position(latLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_minivan));

                mymarker = googlemap.addMarker(markerOptions);
            }
        } else {
            //mymarker.setPosition(latLng);
            if (car_Type == Const.CAR)
                mymarker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_car));
            else if (car_Type == Const.SUV)
                mymarker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_suv));
            else
                mymarker.setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_minivan));
            MarkerAnimation.animateMarkerToGB(mymarker, latLng, new LatLngInterpolator.Spherical());
        }
        if (googlemap != null)
            googlemap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 15));

    }

    private void sendLocationUpdate(Location location) {
        RequestParams params = new RequestParams();
        log(location.getLatitude() + "  " + location.getLongitude());
        params.put("User[lat]", location.getLatitude());
        params.put("User[long]", location.getLongitude());
        params.put("User[direction]", "0.0");
        baseActivity.syncManager.sendToServer(Const.API_USER_SET_LOCATION, params, this);
    }

    @Override
    public void permGranted(int resultCode) {
        if (resultCode == SETUP_MAP_LOCATION_PERM_REQ)
            setCurrentLocation();
    }

    @Override
    public void permDenied(int resultCode) {

    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.setMyLocationEnabled(true);
        //googlemap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMyLocationButtonEnabled(true);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        this.googlemap = googleMap;
        setCurrentLocation();
        startTimer();
    }

    @Override
    public void onSyncStart() {

    }

    @Override
    public void onSyncFinish() {

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("driver") && action.equals("change-availability")) {
            if (status) {
                showToast(jsonObject.optString("message"));
            } else {
                if (jsonObject.has("error")) {
                    showToast(jsonObject.optString("error"));
                } else {
                    showToast(getString(R.string.error));
                }
            }
        }
    }


    @Override
    public void onLocationReceived(Location location) {
        if (location == null)
            return;
        if (firstTimeFlag && googlemap != null) {
            animateCamera(location);
            firstTimeFlag = false;
        }
        setMyLocation(location);

    }

    @Override
    public void onProviderEnabled(String provider) {


    }


    @Override
    public void onProviderDisabled(String provider) {
        if (provider.equalsIgnoreCase(LocationManager.GPS_PROVIDER) && provider.equalsIgnoreCase(LocationManager.NETWORK_PROVIDER))
            buildAlertMessageNoGps();

    }

    private void buildAlertMessageNoGps() {
        if (isAdded()) {
            if (alert == null) {
                alert = new AlertDialog.Builder(baseActivity);
                alert.setMessage(getString(R.string.your_gps_seems_to_be_disable))
                        .setCancelable(false)
                        .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                            public void onClick(@SuppressWarnings("unused") DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                dialog.dismiss();
                                alert = null;
                            }
                        })
                        .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, @SuppressWarnings("unused") int id) {
                                dialog.cancel();
                                alert = null;
                            }
                        });
                alert.show();
            }
        }
    }

    private void startTimer() {
        stopTimer();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(task);
            }
        };
        timer.schedule(timerTask, 30000, 10000);
        //Util.log(TAG,"startTimer timer iniciado");
    }

    public void stopTimer() {
        if (timer != null) {
            timer.cancel();
            if (handler != null)
                handler.removeCallbacks(task);
            timer = null;
            //Util.log(TAG,"stopTimer timer detenido");
        }
    }

    private void animateCamera(@NonNull Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        googlemap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
    }
    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }


    @Override
    public void onPause() {
        super.onPause();
        stopTimer();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopTimer();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (view != null)
            stopTimer();
    }
}
