package com.taxisitio.driver2.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;

import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.pkmmte.view.CircularImageView;
import com.squareup.picasso.Picasso;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.BaseActivity;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.adapter.CustomSpinnerAdapter;
import com.taxisitio.driver2.data.Company;
import com.taxisitio.driver2.data.CountryData;
import com.taxisitio.driver2.data.ProfileData;
import com.taxisitio.driver2.utils.Const;
import com.taxisitio.driver2.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

/**
 * Created by TOXSL\visha.sehgal on 27/1/17.
 */

public class DriverDetailsFragment extends BaseFragment implements ImageUtils.ImageSelectCallback, BaseActivity.PermCallback {
    private static final int CAR_TYPE = 1000;
    private static final int EDIT_MODE = 1001;
    private View view;
    private CircularImageView profileCIV;
    private EditText phoneNumberET;
    ProfileData profileData;
    private MenuItem edit, done;
    private ImageView selectImageIV;
    private Spinner countrySP;
    private int country_id, mType;
    private File file, vehicle_doc_file, license_file, id_proof_file, vehicle_image_file;
    private String[] car_type;
    private String[] edit_mode;
    private int[] car_typeID = new int[3];
    private ArrayList<CountryData> carList = new ArrayList<>();
    private int car_type_id = 0;
    private EditText vehicleRegET;
    private EditText firstNameET;
    private EditText lastNameET;
    private LinearLayout editNameLL;
    private EditText typeOfCarET;
    private EditText modelET;
    private EditText licenseET;
    private EditText vehicleMakeET,etCompany;
    private CircularImageView idProofCIV;
    private CircularImageView drivingLicenseCIV;
    private CircularImageView vehicledocCIV, vehicleImageCIV;
    private EditText emailET;
    private CountryData countryData;
    private Company companySelected;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_details, container, false);
        car_typeID = new int[]{Const.CAR, Const.SUV, Const.Minivan};
        ((MainActivity) getActivity()).setToolbar(getString(R.string.profile));
        edit_mode = new String[]{getString(R.string.profile), getString(R.string.vehicle)};
        setHasOptionsMenu(true);
        getCountryCode();
        init();
        return view;
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        baseActivity.is_profile_edit = false;
        baseActivity.is_vehicle_edit = false;
    }

    private void getCountryCode() {
        baseActivity.syncManager.sendToServer("user/country?id=", null, this);
    }

    private void getProfile() {
        baseActivity.syncManager.sendToServer(Const.API_DRIVER_PROFILE, null, this);
    }

    private void init() {
        profileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        firstNameET = (EditText) view.findViewById(R.id.firstNameET);
        editNameLL = (LinearLayout) view.findViewById(R.id.editNameLL);
        lastNameET = (EditText) view.findViewById(R.id.lastNameET);
        countrySP = (Spinner) view.findViewById(R.id.countrySP);
        phoneNumberET = (EditText) view.findViewById(R.id.phoneNumberET);
        typeOfCarET = (EditText) view.findViewById(R.id.typeOfCarET);
        modelET = (EditText) view.findViewById(R.id.modelET);
        emailET = (EditText) view.findViewById(R.id.emailET);
        vehicleMakeET = (EditText) view.findViewById(R.id.vehicleMakeET);
        licenseET = (EditText) view.findViewById(R.id.licenseET);
        vehicleRegET = (EditText) view.findViewById(R.id.vehicleRegET);
        etCompany = (EditText) view.findViewById(R.id.etCompany);
        idProofCIV = (CircularImageView) view.findViewById(R.id.idProofCIV);
        drivingLicenseCIV = (CircularImageView) view.findViewById(R.id.drivingLicenseCIV);
        vehicledocCIV = (CircularImageView) view.findViewById(R.id.vehicledocCIV);
        vehicleImageCIV = (CircularImageView) view.findViewById(R.id.vehicleImageCIV);
        selectImageIV = (ImageView) view.findViewById(R.id.selectImageIV);
        selectImageIV.setOnClickListener(this);
        typeOfCarET.setOnClickListener(this);
        idProofCIV.setOnClickListener(this);
        drivingLicenseCIV.setOnClickListener(this);
        vehicledocCIV.setOnClickListener(this);
        vehicleImageCIV.setOnClickListener(this);
        profileCIV.setOnClickListener(this);
        getDataUtil().initChoiceCompany(etCompany);


        countrySP.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                CountryData dataItem = (CountryData) parent.getItemAtPosition(position);
                if (!(((CountryData) countrySP.getSelectedItem()).id == 0)) {
                    country_id = dataItem.id;
                    countryData = dataItem;
                    if (countryData.arr_car_type != null) {
                        car_type = new String[countryData.arr_car_type.length];
                        for (int i = 0; i < countryData.arr_car_type.length; i++) {
                            switch (countryData.arr_car_type[i]) {
                                case 0:
                                    car_type[0] = Const.TYPE_CAR;
                                    break;
                                case 1:
                                    if (car_type[0] == null)
                                        car_type[0] = Const.TYPE_SUV;
                                    else
                                        car_type[1] = Const.TYPE_SUV;
                                    break;
                                case 2:
                                    if (car_type[0] == null && car_type[1] == null)
                                        car_type[0] = Const.TYPE_MINIVAN;
                                    else if (car_type[0] != null && car_type[1] == null)
                                        car_type[1] = Const.TYPE_MINIVAN;
                                    else
                                        car_type[2] = Const.TYPE_MINIVAN;
                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int reqCode = -1;
        switch (v.getId()) {
            case R.id.typeOfCarET:
                showOptionsDialog(car_type, CAR_TYPE);
                break;
            case R.id.selectImageIV:
                reqCode = Const.REQ_CODE_PROFILE_IMAGE;
                break;
            case R.id.profileCIV:
                if (!baseActivity.is_profile_edit) {
                    baseActivity.showFullScreenImage(profileData.image_file);
                }
                break;
            case R.id.idProofCIV:
                if (baseActivity.is_vehicle_edit) {
                    reqCode = Const.REQ_CODE_ID_PROOF_IMAGE;
                } else {
                    baseActivity.showFullScreenImage(profileData.id_proof_file);
                }
                break;
            case R.id.vehicledocCIV:
                if (baseActivity.is_vehicle_edit) {
                    reqCode = Const.REQ_CODE_VEHICLE_DOC_IMAGE;
                } else {
                    baseActivity.showFullScreenImage(profileData.document_file);
                }
                break;
            case R.id.drivingLicenseCIV:
                if (baseActivity.is_vehicle_edit) {
                    reqCode = Const.REQ_CODE_LICENCE_IMAGE;
                } else {
                    baseActivity.showFullScreenImage(profileData.license_file);
                }
                break;
            case R.id.vehicleImageCIV:
                if (baseActivity.is_vehicle_edit) {
                    reqCode = Const.REQ_CODE_VEHICLE_IMAGE;
                } else {
                    baseActivity.showFullScreenImage(profileData.vehicle_image_file);
                }
                break;
        }
        if (reqCode != -1) {
            if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, reqCode, this)) {
                selectImage(reqCode);
            }
        }
    }

    private void selectImage(int reqCode) {
        ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, reqCode);
        builder.crop(true).start();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_edit_menu, menu);
        edit = menu.getItem(0);
        done = menu.getItem(1);
        edit.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        done.getIcon().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        if (baseActivity.is_profile_edit) {
            edit.setVisible(false);
            done.setVisible(true);
            baseActivity.is_vehicle_edit = false;
            profileEdit(true);
            vehicleEdit(false);

        } else if (baseActivity.is_vehicle_edit) {
            edit.setVisible(false);
            done.setVisible(true);
            profileEdit(false);
            vehicleEdit(true);
            baseActivity.is_profile_edit = false;
        } else {
            profileEdit(false);
            vehicleEdit(false);
            edit.setVisible(true);
            done.setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit_profile:
                edit.setVisible(false);
                done.setVisible(true);
                showOptionsDialog(edit_mode, EDIT_MODE);
                if (baseActivity.is_profile_edit) {
                    profileEdit(true);
                    vehicleEdit(false);
                } else if (baseActivity.is_vehicle_edit) {
                    profileEdit(false);
                    vehicleEdit(true);

                }
                break;
            case R.id.update_profile:
                if (baseActivity.is_profile_edit) {

                    if (profileValidation()) {
                        edit.setVisible(true);
                        done.setVisible(false);
                        baseActivity.is_profile_edit = false;
                        baseActivity.is_vehicle_edit = false;
                        updateDriverProfileData();
                        profileEdit(false);
                    }
                } else {
                    if (vehicleValidation()) {
                        vehicleEdit(false);
                        edit.setVisible(true);
                        done.setVisible(false);
                        baseActivity.is_profile_edit = false;
                        baseActivity.is_vehicle_edit = false;
                        updateDriverVehicleData();
                    }
                }
                break;
        }

        return super.onOptionsItemSelected(item);

    }

    private void vehicleEdit(boolean edit) {
        etCompany.setEnabled(edit);
        typeOfCarET.setEnabled(edit);
        modelET.setEnabled(edit);
        vehicleMakeET.setEnabled(edit);
        vehicleRegET.setEnabled(edit);
        licenseET.setEnabled(edit);
    }

    private void profileEdit(boolean edit) {
        firstNameET.setEnabled(edit);
        phoneNumberET.setEnabled(edit);
        emailET.setEnabled(edit);
        int maxLengthofEditText = 15;
        if (edit) {
            phoneNumberET.setText(profileData.contact_no);
            countrySP.setVisibility(View.VISIBLE);
            editNameLL.setVisibility(View.VISIBLE);
            selectImageIV.setVisibility(View.VISIBLE);
        } else {
            if(profileData != null){
                phoneNumberET.setText(profileData.phone_code + " " + profileData.contact_no);
            }
            countrySP.setVisibility(View.GONE);
            editNameLL.setVisibility(View.GONE);
            selectImageIV.setVisibility(View.GONE);
            maxLengthofEditText = 40;
        }
        firstNameET.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLengthofEditText)});

    }

    private boolean profileValidation() {
        if (firstNameET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_first_name));
        else if (lastNameET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_last_name));
        else if (emailET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_email));
        else if (!baseActivity.isValidMail(emailET.getText().toString()))
            showToast(getString(R.string.please_enter_valid_email));
        else if (country_id == -1)
            showToast(getString(R.string.please_select_country));
        else if (phoneNumberET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_phone_number));
        else return true;

        return false;

    }

    private boolean vehicleValidation() {
        if (licenseET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_licence_number));
        else if (vehicleRegET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_vehicle_registeration_number));
        else if (car_type_id == 10)
            showToast(getString(R.string.please_select_car_type));
        else if (vehicleMakeET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_car_make));
        else if (modelET.getText().toString().isEmpty())
            showToast(getString(R.string.please_enter_car_model));
        else return true;

        return false;

    }

    public void updateDriverProfileData() {
        RequestParams params = new RequestParams();
        params.put("User[first_name]", firstNameET.getText().toString().trim());
        params.put("User[last_name]", lastNameET.getText().toString().trim());
        params.put("User[contact_no]", phoneNumberET.getText().toString().trim());
        if (file != null)
            try {
                params.put("User[image_file]", file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        if (country_id != 0)
            params.put("User[country_id]", country_id);

        baseActivity.syncManager.sendToServer(Const.API_USER_DRIVER_UPDATE, params, this);
    }

    private void updateDriverVehicleData() {
        RequestParams params = new RequestParams();
        params.put("Driver[vehicle]", car_type_id);
        params.put("Driver[car_make]", vehicleMakeET.getText().toString().trim());
        params.put("Driver[model]", modelET.getText().toString());
        params.put("Driver[license_no]", licenseET.getText().toString());
        params.put("Driver[registration_no]", vehicleRegET.getText().toString());
        if(getDataUtil().getCompanySelected() != null){
            params.put("Driver[company_id]", getDataUtil().getCompanySelected().id);
        }
        if (vehicle_doc_file != null) {
            try {
                params.put("ImageProof[document_file]", vehicle_doc_file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (license_file != null) {
            try {
                params.put(" ImageProof[license_file]", license_file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (id_proof_file != null) {
            try {
                params.put("ImageProof[id_proof_file]", id_proof_file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (vehicle_image_file != null) {
            try {
                params.put("ImageProof[vehicle_image]", vehicle_image_file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        params.put("LoginForm[device_token]", baseActivity.getUniqueDeviceId());
        params.put("LoginForm[device_type]", 1);
        baseActivity.syncManager.sendToServer(Const.API_DRIVER_VEHICLE_UPDATE, params, this);
    }

    private void setDetails() {
        licenseET.setText(profileData.license_no);
        vehicleRegET.setText(profileData.registration_no);
        getDataUtil().loadCompanies(profileData.countryData);
        if (profileData.driver != null && profileData.driver.company != null) {
            etCompany.setText(profileData.driver.company.name);
        }

        Picasso.with(baseActivity).load(profileData.id_proof_file).placeholder(R.mipmap.default_avatar).into(idProofCIV);
        Picasso.with(baseActivity).load(profileData.license_file).placeholder(R.mipmap.default_avatar).into(drivingLicenseCIV);
        Picasso.with(baseActivity).load(profileData.document_file).placeholder(R.mipmap.default_avatar).into(vehicledocCIV);
        Picasso.with(baseActivity).load(profileData.vehicle_image_file).placeholder(R.mipmap.default_avatar).into(vehicleImageCIV);


        carList.clear();
        vehicleMakeET.setText(profileData.car_make);
        typeOfCarET.setText(setCar(Integer.valueOf(profileData.vehicle)));
        modelET.setText(profileData.model);
    }

    private String setCar(Integer type) {
        String car = "";
        switch (type) {
            case 0:
                car = Const.TYPE_CAR;
                break;
            case 1:
                car = Const.TYPE_SUV;
                break;
            case 2:
                car = Const.TYPE_MINIVAN;
                break;
        }
        return car;
    }

    public void buttonTwo(boolean isedit) {
        licenseET.setEnabled(isedit);
        vehicleRegET.setEnabled(isedit);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("driver") && action.equals("profile")) {
            if (status) {
                try {
                    JSONObject detail = jsonObject.getJSONObject("detail");
                    profileData = baseActivity.parseProfileData(detail);
                    baseActivity.saveProfileDataInPrefStore(profileData);
                    if (getActivity() != null)
                        ((MainActivity) getActivity()).updateDrawer();
                    if (baseActivity.is_profile_edit) {
                        firstNameET.setText(profileData.first_name);
                        lastNameET.setText(profileData.last_name);
                    } else {
                        firstNameET.setText(profileData.first_name + " " + profileData.last_name);
                    }
                    emailET.setText(profileData.email);
                    phoneNumberET.setText(profileData.phone_code + " " + profileData.contact_no);
                    for (int i = 0; i < countryDatas.size(); i++) {
                        if (countryDatas.get(i).telephone_code.equals(profileData.phone_code)) {
                            countrySP.setSelection(i);
                            countryData = countryDatas.get(i);
                            break;
                        }
                    }
                    if (countryData != null && countryData.arr_car_type != null) {
                        car_type = new String[countryData.arr_car_type.length];
                        for (int i = 0; i < countryData.arr_car_type.length; i++) {
                            switch (countryData.arr_car_type[i]) {
                                case 0:
                                    car_type[0] = Const.TYPE_CAR;
                                    break;
                                case 1:
                                    if (car_type[0] == null)
                                        car_type[0] = Const.TYPE_SUV;
                                    else
                                        car_type[1] = Const.TYPE_SUV;
                                    break;
                                case 2:
                                    if (car_type[0] == null && car_type[1] == null)
                                        car_type[0] = Const.TYPE_MINIVAN;
                                    else if (car_type[0] != null && car_type[1] == null)
                                        car_type[1] = Const.TYPE_MINIVAN;
                                    else
                                        car_type[2] = Const.TYPE_MINIVAN;
                                    break;
                            }
                        }
                    }
                    if (profileData.image_file != null && (!profileData.image_file.isEmpty())) {
                        CircularImageView prof_drawer = ((MainActivity) baseActivity).profileCIV;
                        baseActivity.picasso.load(profileData.image_file).placeholder(R.mipmap.default_avatar).into(profileCIV);
                        baseActivity.picasso.load(profileData.image_file).placeholder(R.mipmap.default_avatar).into(prof_drawer);
                    }
                    setDetails();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } else if (controller.equals("user") && action.equals("country")) {
            if (status) {
                try {
                    JSONArray country = jsonObject.getJSONArray("country");
                    for (int i = 0; i < country.length(); i++) {
                        JSONObject countryObjct = country.getJSONObject(i);
                        CountryData countryData = new CountryData(Parcel.obtain());
                        countryData.id = countryObjct.getInt("id");
                        countryData.title = countryObjct.getString("title");
                        countryData.telephone_code = countryObjct.getString("telephone_code");
                        countryData.flag = countryObjct.getString("flag");
                        if (countryObjct.has("car_type")) {
                            JSONArray car_types = countryObjct.optJSONArray("car_type");
                            if (car_types.length() != 0) {
                                int[] carTypes = new int[car_types.length()];
                                for (int j = 0; j < car_types.length(); j++) {
                                    carTypes[j] = (int) car_types.opt(j);
                                    countryData.arr_car_type = carTypes;
                                }
                                countryDatas.add(countryData);
                            }
                        }

                    }
                    spinnerAdapter = new CustomSpinnerAdapter(baseActivity, countryDatas, true);
                    countrySP.setAdapter(spinnerAdapter);
                    getProfile();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        } else if (action.equals("driver-update")) {
            if (status) {
                getProfile();
                showToast(getString(R.string.profile_update_successfully));
            } else
                showToast(jsonObject.optString("error"));
        } else if (action.equals("vehicle-update")) {
            if (status) {
                getProfile();
                showToast(getString(R.string.vehicle_detials_update));
            } else
                showToast(jsonObject.optString("error"));
        }
    }

    private ArrayList<CountryData> countryDatas = new ArrayList<>();
    private CustomSpinnerAdapter spinnerAdapter;

    @Override
    public void permGranted(int resultCode) {
        if (resultCode == Const.REQ_CODE_PROFILE_IMAGE
                || resultCode == Const.REQ_CODE_VEHICLE_DOC_IMAGE
                || resultCode == Const.REQ_CODE_LICENCE_IMAGE
                || resultCode == Const.REQ_CODE_ID_PROOF_IMAGE
                || resultCode == Const.REQ_CODE_VEHICLE_IMAGE) {
            selectImage(resultCode);
        }

    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
        Bitmap bitmap = ImageUtils.imageCompress(imagePath);
        if (resultCode == Const.REQ_CODE_PROFILE_IMAGE) {
            file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            profileCIV.setImageBitmap(bitmap);
        } else if (resultCode == Const.REQ_CODE_VEHICLE_DOC_IMAGE) {
            vehicle_doc_file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            vehicledocCIV.setImageBitmap(bitmap);
        } else if (resultCode == Const.REQ_CODE_LICENCE_IMAGE) {
            license_file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            drivingLicenseCIV.setImageBitmap(bitmap);
        } else if (resultCode == Const.REQ_CODE_ID_PROOF_IMAGE) {
            id_proof_file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            idProofCIV.setImageBitmap(bitmap);
        } else if (resultCode == Const.REQ_CODE_VEHICLE_IMAGE) {
            vehicle_image_file = ImageUtils.bitmapToFile(bitmap, baseActivity);
            vehicleImageCIV.setImageBitmap(bitmap);

        }
    }

    public void showOptionsDialog(String[] arr, int type) {
        mType = type;
        AlertDialog.Builder builder = new AlertDialog.Builder(baseActivity, R.style.myDialog);
        builder.setTitle(getString(R.string.select));

        builder.setItems(arr, new DialogInterface.OnClickListener() {


            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                switch (mType) {
                    case CAR_TYPE:
                        typeOfCarET.setText(car_type[i]);
                        switch (typeOfCarET.getText().toString()) {
                            case Const.TYPE_CAR:
                                car_type_id = 0;
                                break;
                            case Const.TYPE_SUV:
                                car_type_id = 1;
                                break;
                            case Const.TYPE_MINIVAN:
                                car_type_id = 2;
                                break;
                        }
                        break;
                    case EDIT_MODE:
                        if (i == 0) {
                            baseActivity.is_profile_edit = true;
                            baseActivity.is_vehicle_edit = false;
                        } else {
                            baseActivity.is_profile_edit = false;
                            baseActivity.is_vehicle_edit = true;
                        }
                        setEditMode();
                        break;
                }

            }
        });
        builder.setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (mType == EDIT_MODE) {
                    edit.setVisible(true);
                    done.setVisible(false);
                }
            }
        });
        builder.show();


    }


    @SuppressLint("SetTextI18n")
    private void setEditMode() {
        if (baseActivity.is_profile_edit) {
            profileEdit(true);
            vehicleEdit(false);
            firstNameET.setText(profileData.first_name);
            lastNameET.setText(profileData.last_name);
        } else if (baseActivity.is_vehicle_edit) {
            firstNameET.setText(profileData.first_name + " " + profileData.last_name);
            profileEdit(false);
            vehicleEdit(true);

        }
    }
}
