package com.taxisitio.driver2.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.data.RideDetails;
import com.taxisitio.driver2.utils.DateUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

/**
 * A simple {@link BaseFragment} subclass.
 * Use the {@link DetailsTripFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetailsTripFragment extends BaseFragment {
    private View view;

    private static final String ARG_RIDE_DETAILS = "RIDE_DETAILS";
    private RideDetails rideDetails;

    public ImageView toolbar_imageIV;
    private TextView  tvPickUpLocation, tvDestination,passengerNameTV,passengerNumberTV,priceTV,tvDate;
    private CircularImageView passengerProfileCIV;

    public DetailsTripFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param rideDetails Parameter 1.
     * @return A new instance of fragment DetailsTripFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DetailsTripFragment newInstance(JSONObject rideDetails) {
        DetailsTripFragment fragment = new DetailsTripFragment();
        Bundle args = new Bundle();
        args.putString(ARG_RIDE_DETAILS, rideDetails.toString());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            JSONObject obj = null;
            try {
                obj = new JSONObject(getArguments().getString(ARG_RIDE_DETAILS));
                rideDetails = baseActivity.parseRideDetails(obj);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_details_trip, container, false);

        toolbar_imageIV = (ImageView) view.findViewById(R.id.toolbar_imageIV);
        tvPickUpLocation = (TextView) view.findViewById(R.id.tvPickUpLocation);
        tvDestination = (TextView) view.findViewById(R.id.tvDestination);
        passengerNameTV = (TextView) view.findViewById(R.id.passengerNameTV);
        passengerProfileCIV = (CircularImageView) view.findViewById(R.id.passengerProfileCIV);
        passengerNumberTV = (TextView) view.findViewById(R.id.passengerNumberTV);
        tvDate = (TextView) view.findViewById(R.id.tvDate);
        priceTV = (TextView) view.findViewById(R.id.priceTV);
        if (rideDetails.passengerDetail.image_file != null && !rideDetails.passengerDetail.image_file.isEmpty()) {
            try {
                baseActivity.picasso.load(rideDetails.passengerDetail.image_file).error(R.mipmap.default_avatar).into(passengerProfileCIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (!rideDetails.amount.equalsIgnoreCase("null") && rideDetails.amount != null && !rideDetails.amount.equals("")) {
            BigDecimal price = new BigDecimal(rideDetails.amount);
            price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
            priceTV.setText(rideDetails.passengerDetail.countryData.currency_symbol.concat(" " + price));
        }
        passengerNameTV.setText(rideDetails.passengerDetail.first_name + " " + rideDetails.passengerDetail.last_name);
        passengerNumberTV.setText(rideDetails.passengerDetail.countryData.telephone_code + " " + rideDetails.passengerDetail.contact_no);

        tvDate.setText(DateUtil.formatDate(baseActivity,rideDetails.create_time));
        toolbar_imageIV.setOnClickListener(this);


        tvPickUpLocation.setText(rideDetails.location);
        tvDestination.setText(rideDetails.destination);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.toolbar_imageIV:
                baseActivity.getSupportFragmentManager().popBackStack();
                break;
        }
    }

}
