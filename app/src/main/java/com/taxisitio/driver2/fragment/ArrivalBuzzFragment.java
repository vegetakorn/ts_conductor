package com.taxisitio.driver2.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.pkmmte.view.CircularImageView;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.BaseActivity;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.data.RideDetails;
import com.taxisitio.driver2.service.LocationUpdateService;
import com.taxisitio.driver2.utils.Const;
import com.taxisitio.driver2.utils.GoogleApisHandle;
import com.taxisitio.driver2.utils.Util;

import org.json.JSONObject;

import java.util.Timer;
import java.util.TimerTask;

import static com.taxisitio.driver2.activity.BaseActivity.googleApiHandle;


/**
 * Vista del conductor donde marca su llegada
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */
public class ArrivalBuzzFragment extends BaseFragment implements BaseActivity.PermCallback, GoogleApisHandle.OnPolyLineReceived, LocationListener, LocationUpdateService.OnLocationReceived {
    private static final String TAG = "ArrivalBuzzFragment";
    private View view;
    private TextView passengerNameTV, pickUpLocationTV, tvDestination;
    private FloatingActionButton callTV, messagesTV, arrivalConfirmTV;
    private CircularImageView passengerProfileCIV;
    private GoogleMap googlemap;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private int rideId;
    private static final int SETUP_MAP_LOCATION_PERM_REQ = 5;
    private RideDetails rideDetails;

    private Location currentLocation;
    int stateId;
    private Timer timer;
    private Handler handler = new Handler();
    private Runnable task = new Runnable() {
        @Override
        public void run() {
            setCurrentLocation();
        }
    };

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_arrival_buzz, container, false);
            ((MainActivity) getActivity()).setToolbar(getString(R.string.app_name));
            initUi();
        }
        if (getArguments() != null && getArguments().containsKey("ride_id")) {
            rideId = getArguments().getInt("ride_id");
        }
        Util.log(TAG, "onCreateView ArrivalBuzzFragment");
        return view;

    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    private void initUi() {
        callTV = (FloatingActionButton) view.findViewById(R.id.callTV);
        messagesTV = (FloatingActionButton) view.findViewById(R.id.messagesTV);
        arrivalConfirmTV = (FloatingActionButton) view.findViewById(R.id.arrivalConfirmTV);
        pickUpLocationTV = (TextView) view.findViewById(R.id.pickUpLocTV);
        tvDestination = (TextView) view.findViewById(R.id.tvDestination);
        passengerProfileCIV = (CircularImageView) view.findViewById(R.id.profileCIV);
        passengerNameTV = (TextView) view.findViewById(R.id.passengerNameTV);
        arrivalConfirmTV.setOnClickListener(this);
        messagesTV.setOnClickListener(this);
        callTV.setOnClickListener(this);
        arrivalConfirmTV.show();


    }

    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            map.getMapAsync(new OnMapReadyCallback() {
                @SuppressLint("MissingPermission")
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    Util.log(TAG, "onMapReady");
                    googleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    googleMap.getUiSettings().setMapToolbarEnabled(false);
                    googleMap.getUiSettings().setCompassEnabled(false);
                    googleMap.getUiSettings().setTiltGesturesEnabled(false);
                    googleMap.getUiSettings().setRotateGesturesEnabled(false);


                    googleMap.setMyLocationEnabled(true);//Se debe habilitar para que se actualice en tiempo real la ubicacion
                    ArrivalBuzzFragment.this.googlemap = googleMap;
                    //startTimer();
                }
            });

        }
        if (rideDetails != null && rideDetails.state_id == Const.STATE_ACCEPTED) {
            drawPolyline();
        } else {
            updatePolyline();
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setCurrentLocation();
        googleApiHandle.setPolyLineReceivedListener(ArrivalBuzzFragment.this);
        if (locationUpdateService != null)
            locationUpdateService.setLocationReceivedListner(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rideDetailsBT:
                break;
            case R.id.arrivalConfirmTV:
                arrivalBuzz();
                break;
            case R.id.callTV:
                call(rideDetails.passengerDetail.countryData.telephone_code.concat(rideDetails.passengerDetail.contact_no));
                break;
            case R.id.messagesTV:
                message(rideDetails.passengerDetail.countryData.telephone_code.concat(rideDetails.passengerDetail.contact_no));
                break;

        }
    }

    @Override
    public void onResume() {
        if (rideId != 0) {
            getRideDetail(getArguments().getInt("ride_id"));
        }
        super.onResume();
    }

    private void arrivalBuzz() {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_ARRIVED + rideId, null, this);
    }

    private void setDriverMarker(LatLng dropoffLatLng, String time, String line) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(setCarType(Integer.parseInt(baseActivity.getProfileDataFromPrefStore().vehicle))));
            if (time != null && !time.isEmpty()) {
                options.title(baseActivity.getString(R.string.estimated_time));
                options.snippet("  " + time + " " + line);
            }
            destinationMarker = googlemap.addMarker(options);
        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle("");
                destinationMarker.setTitle(baseActivity.getString(R.string.estimated_time));
                destinationMarker.setSnippet("  " + time + " " + line);
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();

        }
    }

    private int setCarType(int type) {
        switch (type) {
            case 0:
                return R.mipmap.ic_car;
            case 1:
                return R.mipmap.ic_suv;
            case 2:
                return R.mipmap.ic_minivan;
        }
        return R.mipmap.ic_car;
    }

    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null && pickupLatLng != null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_green)));
        else {
            pickupMarker.setPosition(pickupLatLng);
        }
    }


    private void updatePolyline() {
        if (googlemap != null && rideDetails != null && currentLocation != null) {
            googleApiHandle.getDirectionsUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new LatLng(rideDetails.destination_lat, rideDetails.destination_long), googlemap);
        }
    }


    private void drawPolyline() {
        if (googlemap != null && rideDetails != null && currentLocation != null) {
            googleApiHandle.getDirectionsUrl(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), new LatLng(rideDetails.location_lat, rideDetails.location_long), googlemap);
        }
    }



    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                stateId = rideDetails.state_id;
                rideId = rideDetails.id;
                setState(stateId);
                setUiData();
                initilizeMap();

            } else {
                showToast(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("arrived")) {
            if (status) {
                goToStartRideFragment();
            } else {
                gotoHomeFragment();
                showToast(jsonObject.optString("error"));
            }
        } else {

        }
    }

    private void setState(int stateId) {
        switch (stateId) {
            case Const.STATE_CANCELLED:
                gotoHomeFragment();
                baseActivity.cancelNotification(Const.ALL_NOTI);
                break;
        }
    }

    private void goToStartRideFragment() {
        Fragment frag = new StartRideFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("ride_id", rideId);
        frag.setArguments(bundle);
        baseActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, frag)
                .commit();
    }

    @SuppressLint("SetTextI18n")
    private void setUiData() {
        pickUpLocationTV.setText(rideDetails.location);
        tvDestination.setText(rideDetails.destination);
        passengerNameTV.setText(rideDetails.passengerDetail.first_name + " " + rideDetails.passengerDetail.last_name);
        if (rideDetails.passengerDetail.image_file != null && !rideDetails.passengerDetail.image_file.isEmpty()) {
            try {
                baseActivity.picasso.load(rideDetails.passengerDetail.image_file).error(R.mipmap.default_avatar).into(passengerProfileCIV);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String line) {
        setPickupMarker(destination);
        setDriverMarker(origin, time, line);
    }

    private void call(String number) {
        Intent call = new Intent(Intent.ACTION_CALL);
        call.setData(Uri.parse("tel:" + number));
        try {
            startActivity(call);
        } catch (Exception e) {
            showToast(baseActivity.getString(R.string.no_app_found_to_perform));
        }
    }

    private void message(String number) {
        Intent message = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + number));
        message.putExtra("sms_body", "Type message here");

        try {
            startActivity(message);
        } catch (Exception e) {
            showToast(baseActivity.getString(R.string.app_found_to_perform));
        }
    }


    @Override
    public void onLocationReceived(Location location) {
        currentLocation = location;
        initilizeMap();
        Util.log(TAG,"onLocationReceived Latitude: "+location.getLatitude()+" Longitude: "+location.getLongitude());
    }

    @Override
    public void onLocationChanged(Location location) {
        Util.log(TAG,"onLocationChanged Latitude: "+location.getLatitude()+" Longitude: "+location.getLongitude());
    }

    private void setCurrentLocation() {
        if (baseActivity.checkPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, SETUP_MAP_LOCATION_PERM_REQ, this)) {
            if (ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(baseActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationUpdateService.getFusedLocationClient().getLastLocation()
                    .addOnSuccessListener(new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            currentLocation = location;
                            if (currentLocation != null) {
                                onLocationReceived(currentLocation);
                            } else {
                                showToast(getString(R.string.couldnt_get_current_location));
                            }
                        }
                    });

        } else showToast(baseActivity.getString(R.string.couldnt_get_current_location));

    }

    private void stopTimer() {
        if (timer != null) {
            timer.cancel();
            if (handler != null)
                handler.removeCallbacks(task);
            timer = null;
            Util.log(TAG,"stopTimer timer detenido");
        }
    }

    private void startTimer() {
        stopTimer();
        timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(task);
            }
        };
        timer.schedule(timerTask, 10000, 10000);
        Util.log(TAG,"startTimer timer iniciado");
    }


    @Override
    public void permGranted(int resultCode) {
        if (resultCode == SETUP_MAP_LOCATION_PERM_REQ)
            setCurrentLocation();
    }

    @Override
    public void permDenied(int resultCode) {

    }
}
