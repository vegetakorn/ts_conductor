package com.taxisitio.driver2.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.data.RideDetails;
import com.taxisitio.driver2.utils.Const;
import com.taxisitio.driver2.utils.CustomRegularButton;
import com.taxisitio.driver2.utils.GoogleApisHandle;

import org.json.JSONObject;

import java.math.BigDecimal;

import static com.taxisitio.driver2.activity.BaseActivity.googleApiHandle;


/**
 * Created by TOXSL\visha.sehgal on 17/1/17.
 */

public class RideRequestFragment extends BaseFragment implements OnMapReadyCallback, GoogleApisHandle.OnPolyLineReceived, BaseFragment.JourneyDetailListener {
    private View view;
    private CustomRegularButton rideDetailsBT;
    private TextView petTV, tvPickUpLocation, hrsTV, hourlyTV, noOfBagsTV, tvDestination, priceTV, awayTimeTV, noOfPassengerTV,tvTitle;
    private Button cancelRideBT,bAcceptRide,detailRideBT;
    private LinearLayout hourlyServiceLL, petLL;
    private boolean isRideDetailsVisible;
    private LinearLayout hiddenRideDetailsLL;
    private GoogleMap googlemap;
    private ScrollView scrollSV;
    private CountDownTimer cTimer;
    private long times_in_millies = 60 * 1000;
    private int detailY;
    private Marker pickupMarker;
    private Marker destinationMarker;
    private Polyline polyline;
    private int rideId;
    private RideDetails rideDetails;
    private JSONObject rideDetailsJSON;
    public ImageView toolbar_imageIV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().hide();
        }
        if (view != null) {
            return view;
        } else {
            view = inflater.inflate(R.layout.fragment_ride_request, container, false);

            initUi();
            if (getArguments() != null && getArguments().containsKey("ride_id")) {
                rideId = getArguments().getInt("ride_id");
                getRideDetail(rideId);
            }

            return view;
        }
    }

    private void getRideDetail(int ride_id) {
        baseActivity.syncManager.sendToServer(Const.API_RIDE_DETAIL + "?id=" + ride_id, null, this);
    }

    private void initUi() {
        cancelRideBT = (Button) view.findViewById(R.id.cancelRideBT);
        petTV = (TextView) view.findViewById(R.id.petTV);
        tvTitle = view.findViewById(R.id.tvTitle);
        scrollSV = (ScrollView) view.findViewById(R.id.scrollSV);
        bAcceptRide = (Button) view.findViewById(R.id.bAcceptRide);
        detailRideBT = (Button) view.findViewById(R.id.detailRideBT);
        awayTimeTV = (TextView) view.findViewById(R.id.awayTimeTV);
        priceTV = (TextView) view.findViewById(R.id.priceTV);
        tvDestination = (TextView) view.findViewById(R.id.tvDestination);
        tvPickUpLocation = (TextView) view.findViewById(R.id.tvPickUpLocation);
        noOfPassengerTV = (TextView) view.findViewById(R.id.noOfPassengerTV);
        noOfBagsTV = (TextView) view.findViewById(R.id.noOfBagsTV);
        hourlyTV = (TextView) view.findViewById(R.id.hourlyTV);
        hrsTV = (TextView) view.findViewById(R.id.hrsTV);
        hourlyServiceLL = (LinearLayout) view.findViewById(R.id.hourlyServiceLL);
        petLL = (LinearLayout) view.findViewById(R.id.petLL);
        hiddenRideDetailsLL = (LinearLayout) view.findViewById(R.id.hiddenRideDetailsLL);
        rideDetailsBT = (CustomRegularButton) view.findViewById(R.id.rideDetailsBT);
        toolbar_imageIV = (ImageView) view.findViewById(R.id.toolbar_imageIV);

        rideDetailsBT.setOnClickListener(this);
        bAcceptRide.setOnClickListener(this);
        detailRideBT.setOnClickListener(this);
        cancelRideBT.setOnClickListener(this);
        toolbar_imageIV.setOnClickListener(this);
        int widthSpec = View.MeasureSpec.makeMeasureSpec(scrollSV.getWidth(), View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(scrollSV.getHeight(), View.MeasureSpec.EXACTLY);
        hiddenRideDetailsLL.measure(widthSpec, heightSpec);
        detailY = hiddenRideDetailsLL.getMeasuredHeight();
        hiddenRideDetailsLL.setVisibility(View.GONE);
        startTimer();

    }

    private void initilizeMap() {
        if (googlemap == null) {
            SupportMapFragment map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map));
            if (map != null)
                map.getMapAsync(this);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rideDetailsBT:
                isRideDetailsVisible = !isRideDetailsVisible;
                hiddenRideDetailsLL.setVisibility(isRideDetailsVisible ? View.VISIBLE : View.GONE);
                rideDetailsBT.setText(!isRideDetailsVisible ? getString(R.string.ride_details) : getString(R.string.close));
                if (isRideDetailsVisible)
                    hiddenRideDetailsLL.getParent().requestChildFocus(hiddenRideDetailsLL, hiddenRideDetailsLL);
                else scrollSV.smoothScrollBy(0, 0);
                break;
            case R.id.cancelRideBT:
                cancelRide();
                break;
            case R.id.bAcceptRide:
                acceptRide();
                break;
            case R.id.detailRideBT:
                    gotoDetailsTripFragment();
                break;
            case R.id.toolbar_imageIV:
                baseActivity.getSupportFragmentManager().popBackStack();
                break;
        }
    }

    private void acceptRide() {
        cancelTimer();
        baseActivity.syncManager.sendToServer(Const.API_RIDE_ACCEPT + rideId, null, this);
    }

    private void cancelRide() {
        cancelTimer();
        baseActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new HomeFragment())
                .commitAllowingStateLoss();
    }


    private void setPickupMarker(LatLng pickupLatLng) {
        if (pickupMarker == null)
            pickupMarker = googlemap.addMarker(new MarkerOptions()
                    .position(pickupLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_green)));
        else {
            pickupMarker.setPosition(pickupLatLng);

        }
    }

    private void setDestinationMarker(LatLng dropoffLatLng, String time, String line) {
        if (destinationMarker == null) {
            MarkerOptions options = new MarkerOptions()
                    .position(dropoffLatLng).draggable(false)
                    .icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_map_pin_red));
            if (time != null && !time.isEmpty()) {
                options.title(baseActivity.getString(R.string.estimated_time));
                options.snippet("  " + time + " " + line);
            }
            destinationMarker = googlemap.addMarker(options);

        } else {
            if (time != null && !time.isEmpty()) {
                destinationMarker.setTitle("");
                destinationMarker.setTitle(baseActivity.getString(R.string.estimated_time));
                destinationMarker.setSnippet("  " + time + " " + line);
            }
            destinationMarker.setPosition(dropoffLatLng);
            destinationMarker.showInfoWindow();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("ride") && action.equals("detail")) {
            if (status) {
                rideDetailsJSON = jsonObject.optJSONObject("detail");
                rideDetails = baseActivity.parseRideDetails(rideDetailsJSON);
                initilizeMap();
                rideId = rideDetails.id;
                tvPickUpLocation.setText(rideDetails.location);
                tvDestination.setText(rideDetails.destination);
                if (!rideDetails.amount.equalsIgnoreCase("null") && rideDetails.amount != null && !rideDetails.amount.equals("")) {
                    BigDecimal price = new BigDecimal(rideDetails.amount);
                    price = price.setScale(2, BigDecimal.ROUND_HALF_UP);
                    priceTV.setText(rideDetails.passengerDetail.countryData.currency_symbol.concat(" " + price));
                }

                noOfPassengerTV.setText(rideDetails.number_of_passengers);
                noOfBagsTV.setText(rideDetails.number_of_bags);

                if (rideDetails.is_hourly) {
                    hourlyServiceLL.setVisibility(View.VISIBLE);
                    hrsTV.setText(rideDetails.number_of_hours);
                } else
                    hourlyServiceLL.setVisibility(View.GONE);
                if (rideDetails.is_pet) {
                    petLL.setVisibility(View.VISIBLE);
                    petTV.setText(rideDetails.is_pet ? getString(R.string.yes) : getString(R.string.no));
                } else {
                    petLL.setVisibility(View.GONE);
                }
            } else {
                log(jsonObject.optString("error"));
            }
        } else if (controller.equals("ride") && action.equals("accept")) {
            if (status) {
                cancelTimer();
                RideDetails rideDetails = baseActivity.parseRideDetails(jsonObject.optJSONObject("detail"));
                if (rideDetails.state_id == Const.STATE_ACCEPTED) {
                    Fragment fragment = new ArrivalBuzzFragment();
                    Bundle bundle = new Bundle();
                    bundle.putInt("ride_id", rideDetails.id);
                    fragment.setArguments(bundle);
                    baseActivity.getSupportFragmentManager().beginTransaction()
                            .replace(R.id.container, fragment)
                            .commitAllowingStateLoss();
                }
                log(jsonObject.optString("detail"));
            } else {

                showToast(jsonObject.optString("error"));
                cancelRide();
            }
        } else if (controller.equals("ride") && action.equals("canceled")) {
            if (status) {
                baseActivity.getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, new HomeFragment())
                        .commitAllowingStateLoss();
                log(jsonObject.optString("detail"));
            } else {
                showToast(jsonObject.optString("error"));
            }
        }
        Animation anm = AnimationUtils.loadAnimation(baseActivity, R.anim.shake);
        bAcceptRide.startAnimation(anm);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (googlemap != null) {
            SupportMapFragment startmap = ((SupportMapFragment) getChildFragmentManager()
                    .findFragmentById(R.id.map));
            startmap.onResume();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setCompassEnabled(false);
        googleMap.getUiSettings().setTiltGesturesEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        this.googlemap = googleMap;
        googleApiHandle.setPolyLineReceivedListener(this);
        if (rideDetails != null) {
            googleApiHandle.getDirectionsUrl(new LatLng(rideDetails.location_lat, rideDetails.location_long), new LatLng(rideDetails.destination_lat, rideDetails.destination_long), googleMap);
        }
    }

    public void startTimer() {
        cTimer = new CountDownTimer(times_in_millies, 1000) {
            public void onTick(long millisUntilFinished) {
                int limitTime = 60;
                int seconds = (int) (millisUntilFinished / 1000);
                int minutes = seconds / 60;
                seconds = seconds % 60;
                try {
                    tvTitle.setText(getText(R.string.ride_request)+" ("+seconds+" "+getText(R.string.to_accept_trip)+")");
                    bAcceptRide.setText("("+seconds+") "+getText(R.string.tap_here_to_accept));
                }catch (IllegalStateException e){

                }
            }

            public void onFinish() {
                cancelTimer();
                if (!baseActivity.isFinishing()) {
                    cancelRide();
                }
            }
        };
        cTimer.start();
    }     //cancel timer

    void cancelTimer() {
        if (cTimer != null){
            cTimer.cancel();
            cTimer = null;
        }
    }

    @Override
    public void onPolyLineReceived(LatLng origin, LatLng destination, GoogleMap routeMap, Polyline polyline, double distance, String time, String line) {
        setPickupMarker(origin);
        setDestinationMarker(destination, time, line);
        awayTimeTV.setText(time);
    }

    @Override
    public void updateDetail(RideDetails journeyDetail) {


    }

    private void gotoDetailsTripFragment() {
        Fragment fragment = DetailsTripFragment.newInstance(rideDetailsJSON);
        baseActivity.getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, fragment).addToBackStack(null).commit();

    }
}
