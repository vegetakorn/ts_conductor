package com.taxisitio.driver2.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.MainActivity;
import com.taxisitio.driver2.utils.Const;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONObject;

/**
 * Created by TOXSL\chirag.tyagi on 17/3/17.
 */

public class ChangePasswordFragment extends BaseFragment {
    private View view;
    private EditText currentpwdET, newpwdET, confirmpwdET;
    private Button doneBT;
    private String currentpwd, newpwd, conpwd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_change_password, container, false);
        ((MainActivity) baseActivity).setToolbar(getString(R.string.change_password));
        initUI();
        return view;
    }

    private void initUI() {
        currentpwdET = (EditText) view.findViewById(R.id.currentpwdET);
        newpwdET = (EditText) view.findViewById(R.id.newpwdET);
        confirmpwdET = (EditText) view.findViewById(R.id.confirmpwdET);

        doneBT = (Button) view.findViewById(R.id.doneBT);
        doneBT.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.doneBT:
                validate();
                break;
        }
    }

    private void validate() {
        currentpwd = currentpwdET.getText().toString().trim();
        newpwd = newpwdET.getText().toString().trim();
        conpwd = confirmpwdET.getText().toString().trim();
        if (currentpwd.isEmpty()) {
            showToast(getString(R.string.please_enter_current_password));
        } else if (newpwd.isEmpty()) {
            showToast(getString(R.string.please_enter_new_password));
        } else if (newpwd.length() < 8) {
            showToast(getString(R.string.please_enter_password_of_at_least));
        } else if (!baseActivity.isValidPassword(newpwd)) {
            showToast(getString(R.string.password_should_contain_atleast_1_special_character));
        } else if (conpwd.isEmpty()) {
            showToast(getString(R.string.please_enter_confirm_password));
        } else if (!conpwd.equals(newpwd)) {
            showToast(getString(R.string.dont_match));
        } else {
            hitChangepwdApi();
        }
    }

    private void hitChangepwdApi() {
        RequestParams params = new RequestParams();
        params.put("User[oldPassword]", currentpwd);
        params.put("User[confirm_password]", newpwd);
        params.put("User[newPassword]", conpwd);
        baseActivity.syncManager.sendToServer(Const.API_USER_CHANGE_PASSWORD, params, this);
        baseActivity.startProgressDialog();
    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("change-password")) {
            baseActivity.stopProgressDialog();
            if (status) {
                showToast(getString(R.string.password_change_succesfully));
                gotoHomeFragment();
            } else {
                if (jsonObject.has("error")) {
                    showToast(jsonObject.optString("error"));
                } else {
                    showToast(getString(R.string.error));
                }
            }
        }
    }
}
