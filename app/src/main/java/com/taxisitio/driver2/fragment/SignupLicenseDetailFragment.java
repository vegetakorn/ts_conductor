package com.taxisitio.driver2.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcel;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.pkmmte.view.CircularImageView;
import com.taxisitio.driver2.R;
import com.taxisitio.driver2.activity.BaseActivity;
import com.taxisitio.driver2.data.ProfileData;
import com.taxisitio.driver2.utils.Const;
import com.taxisitio.driver2.utils.ImageUtils;
import com.toxsl.volley.toolbox.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;

/**
 * Created by TOXSL\visha.sehgal on 23/1/17.
 */

public class SignupLicenseDetailFragment extends BaseFragment implements BaseActivity.PermCallback, ImageUtils.ImageSelectCallback {
    private View view;
    private EditText licenceNoET, vehicleRegET;
    private Button uploadInsuranceBT, drivingBT, policeRecordBT, signupBT, inspectionCardBT;
    private File document_file, license_file, id_proof_file, vehicle_image_file;
    private ProfileData signupData = new ProfileData(Parcel.obtain());
    private Location location;
    private CircularImageView idProofIV, vehicledocIV, licenseIV, vehicleImageIV;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (view == null) {
            view = inflater.inflate(R.layout.fragment_signup_license_details, container, false);
            if (getArguments().containsKey("signupdata")) {
                signupData = getArguments().getParcelable("signupdata");
            } else if (getArguments().containsKey("id")) {
                signupData.id = String.valueOf(getArguments().getInt("id"));
            }
            init();
        }
        return view;
    }

    private void init() {
        licenceNoET = (EditText) view.findViewById(R.id.licenceNoET);
        vehicleRegET = (EditText) view.findViewById(R.id.vehicleRegET);
        idProofIV = (CircularImageView) view.findViewById(R.id.idProofIV);
        vehicledocIV = (CircularImageView) view.findViewById(R.id.vehicledocIV);
        vehicleImageIV = (CircularImageView) view.findViewById(R.id.vehicleImageIV);
        licenseIV = (CircularImageView) view.findViewById(R.id.licenseIV);
        signupBT = (Button) view.findViewById(R.id.signupBT);

        signupBT.setOnClickListener(this);

        idProofIV.setOnClickListener(this);
        vehicledocIV.setOnClickListener(this);
        licenseIV.setOnClickListener(this);
        vehicleImageIV.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.signupBT:
                if (validation())
                    signup();

                break;
            case R.id.vehicledocIV:// insaurance
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 123, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 123);
                    builder.crop(true).start();
                }
                break;
            case R.id.licenseIV:// license
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 123, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 456);
                    builder.crop(true).start();
                }
                break;
            case R.id.idProofIV://police record
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 123, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 789);
                    builder.crop(true).start();
                }
                break;
            case R.id.vehicleImageIV://vehicle Image
                if (baseActivity.checkPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 123, this)) {
                    ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, 890);
                    builder.crop(true).start();
                }
                break;

        }
    }

    @SuppressLint("MissingPermission")
    private void signup() {
        locationUpdateService.getFusedLocationClient().getLastLocation()
                .addOnSuccessListener(new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location2) {
                        location = location2;
                    }
                });

        String token = FirebaseInstanceId.getInstance().getToken();
        RequestParams params = new RequestParams();
        params.put("Driver[license_no]", licenceNoET.getText().toString().trim());
        params.put("Driver[registration_no]", vehicleRegET.getText().toString().trim());
        if (token != null)
            params.put("LoginForm[device_token]", token);
        else
            params.put("LoginForm[device_token]", baseActivity.getUniqueDeviceId());
        params.put("LoginForm[device_type]", "1");
        try {
            params.put("ImageProof[document_file]", document_file);
            params.put("ImageProof[license_file]", license_file);
            params.put("ImageProof[id_proof_file]", id_proof_file);
            params.put("ImageProof[vehicle_image]", vehicle_image_file);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        baseActivity.syncManager.sendToServer(Const.API_STEP_2 + "?id=" + signupData.id, params, this);

    }

    private boolean validation() {
        if (licenceNoET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_licence_number));
        else if (vehicleRegET.getText().toString().trim().isEmpty())
            showToast(getString(R.string.please_enter_vehicle_registeration_number));
        else if (vehicle_image_file == null)
            showToast(getString(R.string.please_select_car_image));
        else if (document_file == null)
            showToast(getString(R.string.please_add_insurance_details));
        else if (license_file == null)
            showToast(getString(R.string.please_add_driving_licence_image));
        else if (id_proof_file == null)
            showToast(getString(R.string.please_add_police_record_image));
        else return true;
        return false;
    }

    @Override
    public void permGranted(int resultCode) {
        if (resultCode == 123 || resultCode == 456 || resultCode == 789 || resultCode == 890) {
            ImageUtils.ImageSelect.Builder builder = new ImageUtils.ImageSelect.Builder(baseActivity, this, resultCode);
            builder.crop(true).start();
        }


    }

    @Override
    public void permDenied(int resultCode) {

    }

    @Override
    public void onImageSelected(String imagePath, int resultCode) {
        Bitmap bitmap = ImageUtils.imageCompress(imagePath);
        if (resultCode == 123) {
            document_file = new File(imagePath);
            vehicledocIV.setImageBitmap(bitmap);
        } else if (resultCode == 456) {
            license_file = new File(imagePath);// ImageUtils.bitmapToFile(bitmap, baseActivity);
            licenseIV.setImageBitmap(bitmap);
        } else if (resultCode == 789) {
            id_proof_file = new File(imagePath);
            idProofIV.setImageBitmap(bitmap);
        } else if (resultCode == 890) {
            vehicle_image_file = new File(imagePath);
            vehicleImageIV.setImageBitmap(bitmap);

        }

    }

    @Override
    public void onSyncSuccess(String controller, String action, boolean status, JSONObject jsonObject) {
        super.onSyncSuccess(controller, action, status, jsonObject);
        if (controller.equals("user") && action.equals("driver-step2")) {
            if (status) {
                showToast(getString(R.string.signp_successful));
                JSONObject object = null;
                try {
                    object = jsonObject.optJSONObject("detail");
                    baseActivity.saveProfileDataInPrefStore(baseActivity.parseProfileData(object));
                    baseActivity.syncManager.setLoginStatus(null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                goToLoginSignUpScreen();

            } else showToast(jsonObject.optString("error"));
        }
    }

    private void goToLoginSignUpScreen() {
        baseActivity.getSupportFragmentManager().beginTransaction().replace(R.id.frame_container, new LoginSignupFragment()).commit();
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }
}
