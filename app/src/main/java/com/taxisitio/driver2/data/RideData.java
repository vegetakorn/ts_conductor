package com.taxisitio.driver2.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\visha.sehgal on 27/1/17.
 */

public class RideData implements Parcelable {

    public int type_id;
    public int state_id;
    public int base_type;
    public int payment_mode;
    public int number_of_passengers;
    public int number_of_bags;
    public String id;
    public String charge_mode;
    public String amount;
    public String destinationAddress;
    public String pickupAddress;
    public String journeyTime;
    public double pickupLat;
    public double destinationLat;
    public double destinationLong;
    public double pickupLong;

    public RideData(Parcel in) {
        type_id = in.readInt();
        state_id = in.readInt();
        base_type = in.readInt();
        payment_mode = in.readInt();
        number_of_passengers = in.readInt();
        number_of_bags = in.readInt();
        id = in.readString();
        charge_mode = in.readString();
        amount = in.readString();
        destinationAddress = in.readString();
        pickupAddress = in.readString();
        journeyTime = in.readString();
        pickupLat = in.readDouble();
        destinationLat = in.readDouble();
        destinationLong = in.readDouble();
        pickupLong = in.readDouble();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(type_id);
        dest.writeInt(state_id);
        dest.writeInt(base_type);
        dest.writeInt(payment_mode);
        dest.writeInt(number_of_passengers);
        dest.writeInt(number_of_bags);
        dest.writeString(id);
        dest.writeString(charge_mode);
        dest.writeString(amount);
        dest.writeString(destinationAddress);
        dest.writeString(pickupAddress);
        dest.writeString(journeyTime);
        dest.writeDouble(pickupLat);
        dest.writeDouble(destinationLat);
        dest.writeDouble(destinationLong);
        dest.writeDouble(pickupLong);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<RideData> CREATOR = new Creator<RideData>() {
        @Override
        public RideData createFromParcel(Parcel in) {
            return new RideData(in);
        }

        @Override
        public RideData[] newArray(int size) {
            return new RideData[size];
        }
    };
}
