package com.taxisitio.driver2.data;

import com.google.gson.annotations.SerializedName;

/**
 * Empresa
 */
public class Company {
    @SerializedName("id")
    public int id;

    @SerializedName("name")
    public String name;
}
