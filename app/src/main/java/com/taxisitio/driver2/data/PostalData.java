package com.taxisitio.driver2.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\ankan.tiwari on 26/9/17.
 */

public class PostalData implements Parcelable {

    int postal_code;


    protected PostalData(Parcel in) {
        postal_code = in.readInt();
    }


    public static final Creator<PostalData> CREATOR = new Creator<PostalData>() {
        @Override
        public PostalData createFromParcel(Parcel in) {
            return new PostalData(in);
        }

        @Override
        public PostalData[] newArray(int size) {
            return new PostalData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(postal_code);
    }
}
