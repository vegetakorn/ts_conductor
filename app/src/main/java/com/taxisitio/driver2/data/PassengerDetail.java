package com.taxisitio.driver2.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by TOXSL\jagmel.singh on 3/6/17.
 */

public class PassengerDetail implements Parcelable {
    public int id;
    public String first_name;
    public String last_name;
    public String email;
    public String contact_no;
    public String state_id;
    public int passenger_rating;
    public double location_lat;
    public String image_file;
    public CountryData countryData;


    public PassengerDetail(Parcel in) {
        id = in.readInt();
        first_name = in.readString();
        last_name = in.readString();
        email = in.readString();
        contact_no = in.readString();
        state_id = in.readString();
        passenger_rating = in.readInt();
        location_lat = in.readDouble();
        image_file = in.readString();
        countryData = in.readParcelable(CountryData.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(first_name);
        dest.writeString(last_name);
        dest.writeString(email);
        dest.writeString(contact_no);
        dest.writeString(state_id);
        dest.writeInt(passenger_rating);
        dest.writeDouble(location_lat);
        dest.writeString(image_file);
        dest.writeParcelable(countryData, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<PassengerDetail> CREATOR = new Creator<PassengerDetail>() {
        @Override
        public PassengerDetail createFromParcel(Parcel in) {
            return new PassengerDetail(in);
        }

        @Override
        public PassengerDetail[] newArray(int size) {
            return new PassengerDetail[size];
        }
    };
}

