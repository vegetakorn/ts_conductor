package com.taxisitio.driver2.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.taxisitio.driver2.R;
import com.taxisitio.driver2.data.DrawerData;

import java.util.ArrayList;

/**
 * Created by TOXSL\visha.sehgal on 24/1/17.
 */

public class NavigationAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<DrawerData> navDrawerItems;

    public NavigationAdapter(Context context, ArrayList<DrawerData> navDrawerItems) {
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItem;
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        listItem = inflater.inflate(R.layout.adapter_drawer, parent, false);
        ImageView imageViewIcon = (ImageView) listItem.findViewById(R.id.iconIV);
        TextView textViewName = (TextView) listItem.findViewById(R.id.passengerNameTV);
        textViewName.setText(navDrawerItems.get(position).name);
        imageViewIcon.setImageResource(navDrawerItems.get(position).icon);
        return listItem;
    }
}